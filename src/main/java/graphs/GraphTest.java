package graphs;

import graphics.GraphType;
import graphics.Graphic;

public class GraphTest {
	public static void main(String[] args) {

		Graph g = new GraphAM();
		g.addVertex(3);
		g.addVertex(8);
		g.addVertex(5);
		g.addVertex(1);
		g.addVertex(9);
		g.addVertex(11);
		g.addVertex(55);
		//g.addVertex(8);
		g.addEdge(3, 8);
		g.addEdge(8, 5);
		g.addEdge(8, 1);
		g.addEdge(5, 1);
		g.addEdge(5, 9);
		g.addEdge(1, 9);
		g.addEdge(1, 11);
		g.addEdge(3,55);
		g.changeValueOfEdge(3, 8, 2);
		g.changeValueOfEdge(8, 5, 2);
		g.changeValueOfEdge(8, 1, 3);
		g.changeValueOfEdge(5, 1, 4);
		g.changeValueOfEdge(5, 9, 5);
		g.changeValueOfEdge(3, 55, 5);
		//g.changeValueOfEdge(1, 9, 1);
//		g.printGraph();
//		g.write();

		PrimsAlgorithm prim = new PrimsAlgorithm((GraphAM) g, 8);
		
		/* Graphic Gr=new Graphic(GraphType.GRAPH);
		 Gr.addEdge(4, 5, 1);
		 Gr.addEdge(5, 7, 2);
		 Gr.addEdge(7, 9, 1);
		 Gr.addEdge(8, 9, 1);
		 Gr.addEdge(1, 8, 4);
		 Gr.addEdge(1, 5, 3);
		 Gr.printGraph("Graf1");
		 */

		// Graph a = new GraphAL();
		// a.addVertex(5);
		// a.addVertex(2);
		// a.addVertex(8);
		// a.addVertex(7);
		// a.addEdge(5, 7);
		// a.addEdge(5, 2);
		// a.addEdge(5, 8);
		// a.addEdge(3, 8);
		// a.addEdge(8, 2);
		// a.addEdge(2, 8);
		// a.changeValueOfEdge(5, 7, 2);
		// a.printGraph();
		// a.write();
	}

}
