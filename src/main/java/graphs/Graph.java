package graphs;


public interface Graph {
    public boolean addVertex(int vertex);
    public boolean deleteVertex(int vertex);
    public boolean addEdge(int vertex0,int vertex1);
    public boolean deleteEdge(int vertex0,int vertex1);
    public void write();
    public void read();
    public void printGraph();
    public void printEdges();
    public int changeValueOfEdge(int vertex0,int vertex1, int value);
}
