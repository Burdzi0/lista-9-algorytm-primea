package graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import graphics.GraphType;
import graphics.Graphic;

public class PrimsAlgorithm {

	int[][] graph;
	// Lista gdzie bed� sprawdzone wierzchki
	ArrayList<Integer> T = new ArrayList<Integer>();
	ArrayList<Path> queue = new ArrayList<Path>();
	ArrayList<Path> wzieteSciezki = new ArrayList<Path>();
	ArrayList<Path> dlaGraf=new ArrayList<Path>();
	//ArrayList<Path> dlaCal=new ArrayList<Path>();
	int ileWierzcholkow;

	public PrimsAlgorithm(GraphAM graf, int startFrom) {

		graph = graf.getGraph();
		for (int i = 0; i < graph[0].length; i++) {
			if (graph[0][i] != -1)
				ileWierzcholkow++;
		}

		if (graph[0][startFrom] == -1) {
			System.out.println("nie ma takiego wierzchoka");
			return;
		}

		int vertex = startFrom;
		T.add(vertex);
		Path dodatkowe = null;
		// int z=0;
		// while(z<2){
		while (T.size() < ileWierzcholkow) {
			if (dodatkowe != null) {
				if ((T.indexOf(dodatkowe.vertex1)) != -1) {
					// TUTAJ DRUKUJESZ ZE NIE ROZPATRUJEMY TEJ KRAWEDZI BO TEN WIERZCHOEK JUZ JEST W
					// "T"

					dodatkowe = queue.get(0);
					queue.remove(0);
					continue;
				}
				// TUTAJ JEST ZE DODAJEMY WIERZCHOLKI DO T, TEGO CHYBA NIE MUSZ DRUKOWAC
				T.add(dodatkowe.vertex1);
				vertex = dodatkowe.vertex1;
				wzieteSciezki.add(dodatkowe);
			}

			while (areEdges(vertex).value != 0) {
				// TUTAJ SOBIE SPRWADZMY JAKIE MAMY KRAWEDZIE DO DYSPOZYCJI W DANYM MOMENCIE
				Path temp = areEdges(vertex);
				// I TUTAJ SOBIE JE DODAJEMY
				queue.add(temp);
				dlaGraf.add(temp);
				graph[temp.vertex1][temp.vertex0] = 0;
				graph[temp.vertex0][temp.vertex1] = 0;
				// G3.addEdge(temp.vertex0, temp.vertex1, temp.value);
				// System.out.println(temp.vertex1);
			}

			// Sortowanie wzgledem value
			Collections.sort(queue, getCompByName());
			if (queue.size() == 0) {
				break;
			}
			dodatkowe = queue.get(0);
			queue.remove(0);

			// z++;
		}
		drukG();
		drukGR();
//		printGraph();

//		for (int i = 0; i < T.size(); i++)
//			System.out.println(T.get(i));
//		System.out.println();
//		for (int i = 0; i < wzieteSciezki.size(); i++) {
//			System.out.println(wzieteSciezki.get(i).vertex0 + "-->" + wzieteSciezki.get(i).vertex1 + " value: "
//					+ wzieteSciezki.get(i).value);
//		}
		drukR();
	}

	public Path areEdges(int vertex) {
		Path jest = new Path();
		jest.value = 0;
		jest.vertex0 = vertex;
		for (int i = 1; i < graph[vertex].length; i++) {
			if (graph[vertex][i] != 0) {
				jest.value = graph[vertex][i];
				jest.vertex1 = i;
				break;
			}
		}
		return jest;
	}

	public static Comparator<Path> getCompByName() {
		Comparator comp = new Comparator<Path>() {
			@Override
			public int compare(Path s1, Path s2) {
				return s1.value - s2.value;
			}
		};
		return comp;
	}

	public void printGraph() {
		for (int i = 0; i < graph.length; i++) {
			for (int j = 0; j < graph[0].length; j++) {
				System.out.printf("%3d", graph[i][j]);
			}
			System.out.println();
		}
		System.out.println();
	}

	public void drukG() {
		/*Graphic G1 = new Graphic(GraphType.GRAPH);
		for (int i = 0; i < dlaGraf.size(); i++) {
			G1.addEdge(dlaGraf.get(i).vertex0, dlaGraf.get(i).vertex1, dlaGraf.get(i).value);
		}
		G1.printGraph("WynikAlgorytmu(graf)");*/
		int check = 0;
		Graphic G1 = new Graphic(GraphType.GRAPH);
		for (int i = 0; i < wzieteSciezki.size(); i++) {
			G1.addEdge(wzieteSciezki.get(i).vertex0, wzieteSciezki.get(i).vertex1, wzieteSciezki.get(i).value);
			new Graphic(G1).printGraph(String.valueOf(i));
		}
		for (int i = 0; i < dlaGraf.size(); i++) {
			for (int j = 0; j < wzieteSciezki.size(); j++) {
				if (dlaGraf.get(i).checkIfEqual(wzieteSciezki.get(j))) {
					check = 1;
					continue;
				}
			}
			if (check == 0) {
				G1.addEdge(dlaGraf.get(i).vertex0, dlaGraf.get(i).vertex1, dlaGraf.get(i).value);
				new Graphic(G1).printGraph(String.valueOf(i));
			}
			check = 0;
		}
		G1.printGraph("WynikAlgorytmu(graf)");
	}

	public void drukR() {
		;
		Graphic G2 = new Graphic(GraphType.GRAPH);
		for (int i = 0; i < wzieteSciezki.size(); i++) {
			G2.addRedEdge(wzieteSciezki.get(i).vertex0, wzieteSciezki.get(i).vertex1, wzieteSciezki.get(i).value);
			new Graphic(G2).printGraph(String.valueOf(i));
		}
//		G2.printGraph("WynikAlgorytmu(rozpietosc)");
	}

	public void drukGR() {
		int check = 0;
		Graphic G3 = new Graphic(GraphType.GRAPH);
		for (int i = 0; i < wzieteSciezki.size(); i++) {
			G3.addRedEdge(wzieteSciezki.get(i).vertex0, wzieteSciezki.get(i).vertex1, wzieteSciezki.get(i).value);
			new Graphic(G3).printGraph(String.valueOf(i));
		}
		for (int i = 0; i < dlaGraf.size(); i++) {
			for (int j = 0; j < wzieteSciezki.size(); j++) {
				if (dlaGraf.get(i).checkIfEqual(wzieteSciezki.get(j))) {
					check = 1;
					continue;
				}
			}
			if (check == 0) {
				G3.addEdge(dlaGraf.get(i).vertex0, dlaGraf.get(i).vertex1, dlaGraf.get(i).value);
				new Graphic(G3).printGraph(String.valueOf(++i));
			}
			check = 0;
		}
		G3.printGraph("WynikAlgorytmu(rozpietosc+graf)");
	}

}
