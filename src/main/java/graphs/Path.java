package graphs;

public class Path {
	public int value;
	public int vertex0;
	public int vertex1;

	public Path() {
		value = 0;
		vertex0 = 0;
		vertex1 = 0;
	}
	public boolean checkIfEqual(Path b) {
		if(value==b.value&&vertex0==b.vertex0&&vertex1==b.vertex1)
			return true;
		else return false;
	}
}
