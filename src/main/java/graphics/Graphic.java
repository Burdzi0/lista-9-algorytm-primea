package graphics;

import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;
import guru.nidi.graphviz.model.MutableGraph;
import guru.nidi.graphviz.parse.Parser;

import java.io.File;
import java.io.IOException;

public class Graphic<T> {

    private StringBuilder graph = new StringBuilder();
    private String connector;
    private String semicolon = ";";

    public Graphic(GraphType graphType) {
        graph.append(graphType.getType());
        connector = graphType.getConnector();
        graph.append(" {").append(System.lineSeparator());
    }

    public Graphic(Graphic graphic) {
        this.connector = graphic.connector;
        this.graph = new StringBuilder(graphic.graph.toString());
    }

    private String surroundWithDoubleQuotes(T value) {
        return "\"" + value.toString() + "\"";
    }

    public void addEdge(T from, T to, T weight) {
        graph.append(surroundWithDoubleQuotes(from))
                .append(connector)
                .append(surroundWithDoubleQuotes(to))
                .append(addLabel(weight.toString()))
                .append(System.lineSeparator());
    }

    public void addRedEdge(T from, T to, T weight) {
        graph.append(from.toString())
                .append(connector)
                .append(to.toString())
                .append(addLabelToRedEdge(weight.toString()))
                .append(System.lineSeparator());
    }

    private String addLabel(String label) {
        return " [\"label\"=\"" + label + "\"]";
    }

    private String addLabelToRedEdge(String label) {
        return " [\"label\"=\"" + label + "\" \"color\"=\"red\"]";
    }

    public void printGraph(String fileName) {
        graph.append("}");
        String graphText = graph.toString();
        MutableGraph mutableGraph = null;
        try {
            mutableGraph = Parser.read(graphText);
            if (mutableGraph == null) {
                System.out.println("The graph is empty or invalid!");
            }
            Graphviz.fromGraph(mutableGraph)
                    .render(Format.PNG)
                    .toFile(new File("results/" + fileName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

     public void printGraph(String fileName, int width, int height) {
        String graphText = graph.toString();
        MutableGraph mutableGraph = null;
        try {
            mutableGraph = Parser.read(graphText);
            if (mutableGraph == null) {
                System.out.println("The graph is empty or invalid!");
            }
            Graphviz.fromGraph(mutableGraph)
                    .width(width)
                    .height(height)
                    .render(Format.PNG).toFile(new File("results/" + fileName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
