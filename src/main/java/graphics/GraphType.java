package graphics;

public enum GraphType {
    GRAPH("graph", " -- "), DIGRAPH("digraph", " -> ");

    private final String digraph;
    private final String connector;

    GraphType(String digraph, String connector) {
        this.digraph = digraph;
        this.connector = connector;
    }

    public String getType() {
        return digraph;
    }

    public String getConnector() {
        return connector;
    }
}
