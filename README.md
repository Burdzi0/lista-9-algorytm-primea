# Lista 9 - Algorytm Prime #

Projekt jest tworzony przez 3 osoby:

* Filip Löffler
* Patryk Czekała
* Łukasz Burdyna - właściciel repo

### Przeczytaj zanim zrobisz commita ###

* Każdy pracuje na własnym branchu
* Merga robimy kiedy jest potrzebny
* Trzeba się jeszcze robotą podzielić

### Komunikat Maven'a ###
Zapewne podczas uruchomienia projektu w IDE wyświetli się komunikat:
```java
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
```
Wynika to z braku pewnych zależności w pliku `pom.xml`.
Dodanie ich powoduje wyświetlenie się kolejnego błędu, co prowadzi do swego rodzaju `error-hell`.
Brak tej zależności jest niegroźny, ponieważ dotyczy tylko loggera `SLF4J`, który w projekcie **nie jest wykorzystywany**


### Kontakt ###
Właściciel tego repo - burdzi0<małpka>programmer.net